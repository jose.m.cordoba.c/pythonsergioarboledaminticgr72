"""Programa que busca los mejores estudiantes
"""
lista_estudiantes = {"viviana": 7, "andrea": 5, "jose": 4, "manuel": 1, "pedro": 2,
                     "paula": 8, "juliana": 3, "carlos": 7, "camila": 6, "Fernanda": 7,
                     "juan": 9, "juana": 3, "Dora": 2, "santiago": 9, "moncho": 8,
                     "Karla": 9, "kathe": 3, "Andres": 2, "Mclovin": 9, "Madona": 10}

los_mejores = []
los_motivados = []

# Para cada estudiante en la lista de estudiantes
for estudiante in lista_estudiantes:

    # Obtengo el promedio del estudiante
    promedio_estudiante = lista_estudiantes[estudiante]

    # Si el promedio del estudiante es mayor o igual que 9
    if promedio_estudiante >= 9:
        # Agregar el estudiante a la lista de los mejores
        los_mejores.append(estudiante)

    # Si no
    else:
        # Agregar el estudiante a la lista de motivados
        los_motivados.append(estudiante)


print(f'Los mejores estudiantes son: {los_mejores}')
print(f'Los decepcionantes son: {los_motivados}')
