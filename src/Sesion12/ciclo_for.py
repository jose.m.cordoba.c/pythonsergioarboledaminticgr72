"""Ejemplos con el ciclo for
    """

# Imprimir los numeros del 0 al 9
for numero in range(10):
    print(numero)

# Imprimir todas las letras en una frase
for letra in "Hola mundo":
    print(letra)

# Imprimimamos los numeros del -10 al 20
for numero in range(-10, 20):
    print(numero)

# Imprimimamos los numeros del 10 al -1
for numero in range(10, 0, -1):
    print(numero)

# Imprimir los numeros pares del 50 y 200
for num in range(50, 200, 2):
    print(num)

# Imprimir los numeros pares del 50 y 200
for num in range(49, 141, 7):
    print(num)
