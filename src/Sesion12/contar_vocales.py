"""programa para contar las vocales en un texto
    """

VOCALES = 'AEIOUaeiou'


def contar_vocales(frase: str) -> int:
    """[summary]

    >>> contar_vocales('Hola Mundo')
    4

    >>> contar_vocales('Jose Manuel Cordoba Castillo')
    11

    Args:
        frase (str): [description]

    Returns:
        int: [description]
    """
    contador = 0

    for letra in frase:
        if es_vocal(letra):
            contador += 1

    return contador


def es_vocal(letra: str) -> bool:
    """[summary]

    >>> es_vocal('A')
    True

    >>> es_vocal('e')
    True

    >>> es_vocal('AE')
    False

    >>> es_vocal('Z')
    False

    >>> es_vocal('p')
    False

    Args:
        letra (str): [description]

    Returns:
        bool: [description]
    """
    return len(letra) == 1 and letra in VOCALES
