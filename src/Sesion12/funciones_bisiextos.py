"""Programa para encontrar años bisiestos multiplos de 10
"""


def es_multiplo(numero: int, multiplo: int) -> bool:
    """Determina si un numero es multiplo de otro numero

    >>> es_multiplo(963, 3)
    True

    >>> es_multiplo(98, 7)
    True

    >>> es_multiplo(79, 4)
    False

    Args:
        numero (int): [description]
        multiplo (int): [description]

    Returns:
        bool: [description]
    """
    return numero % multiplo == 0


def es_bisiesto(año: int) -> bool:
    """Determina si un año es bisiexto

    >>> es_bisiesto(2000)
    True

    >>> es_bisiesto(1996)
    True

    >>> es_bisiesto(100)
    False

    >>> es_bisiesto(2024)
    True

    >>> es_bisiesto(2021)
    False

    Args:
        año (int): [description]

    Returns:
        bool: [description]
    """
    return es_multiplo(año, 400) or (not es_multiplo(año, 100) and es_multiplo(año, 4))
