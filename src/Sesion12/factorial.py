"""Programa para calcular un factorial
    """


def factorial(numero: int) -> int:
    """Calcula el factorial del numero

    >>> factorial(0)
    1

    >>> factorial(3)
    6

    >>> factorial(5)
    120

    Args:
        numero (int): Un numero entero positivo

    Returns:
        int: el factorial calculado
    """
    acumulador = 1
    for n in range(1, numero + 1):
        acumulador *= n
    return acumulador


print(factorial(100))
print(factorial(76))
