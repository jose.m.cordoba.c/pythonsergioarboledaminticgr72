"""Programa que suma los numeros negativos y hace el promedio de los positivos
    """


def sumar_y_restar(*parametros) -> str:
    """[summary]

    >>> sumar_y_restar(5, 2, 0, -4, 3, -2)
    '-6 2.5'

    >>> sumar_y_restar(-5, -2, 0, -4, -3, -2)
    '-16 0.0'

    >>> sumar_y_restar(5, 5, 5, 5, 5, 5)
    '0 5.0'

    Returns:
        str: [description]
    """
    suma_negativos, suma_positivos, contador_positivos = 0, 0, 0
    for parametro in parametros:
        if parametro >= 0:
            suma_positivos += parametro
            contador_positivos += 1
        else:
            suma_negativos += parametro

    promedio_positivos = 0
    if contador_positivos != 0:
        promedio_positivos = suma_positivos / contador_positivos
    return f'{suma_negativos} {promedio_positivos}'
