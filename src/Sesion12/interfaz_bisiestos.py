"""Interacciones con el usuario para el ejemplo de bisiestos
"""

from funciones_bisiextos import es_bisiesto, es_multiplo

año_inicial, año_final = input(
    'Ingrese el rango de años separado por espacio ').split()
año_inicial = int(año_inicial)
año_final = int(año_final)

for año in range(año_inicial, año_final):
    if es_multiplo(año, 10) and es_bisiesto(año):
        print(f'el {año} es bisiesto y multiplo de 10')
