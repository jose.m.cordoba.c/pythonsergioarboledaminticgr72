# Datos base
productos = {
    1: ['Naranjas', 7000.0, 35],
    2: ['Limones', 2500.0, 15],
    3: ['Peras', 2700.0, 65],
    4: ['Arandanos', 9300.0, 34],
    5: ['Tomates', 2100.0, 42],
    6: ['Fresas', 9100.0, 20],
    7: ['Helado', 4500.0, 41],
    8: ['Galletas', 500.0, 8],
    9: ['Chocolate', 4500.0, 80],
    10: ['Jamon', 17000.0, 48]
}


# Entrada
operacion = input()
codigo, nombre, precio, inventario = input().split()

# Proceso

# decidir que operacion se solicitó
# realizar la operacion correspondiente

precio_mayor = obtener_mayor_precio()
nombre_menor = obtener_nombre_menor_precio()
promedio_precios = calcular_promedio_precios
valor_inventario = calcular_valor_inventario()


def agregar_producto(codigo, nombre, precio, inventario):
    pass


def actualizar_producto(codigo, nombre, precio, inventario):
    pass


def borrar_producto(codigo, nombre, precio, inventario):
    pass


def calcular_valor_inventario():
    pass


def obtener_mayor_precio():
    pass


def obtener_nombre_menor_precio():
    pass


def calcular_promedio_precios():
    pass


# Salida
print(precio_mayor, nombre_menor, promedio_precios, valor_inventario)
