"""Ejemplos de operadores de comparación
    """

valor1 = 87
valor2 = 65

# Comparar si son iguales
print(
    f'Si valor_1 es {valor1} y valor_2 es {valor2} la comparacion de igualdad es {valor1 == valor2}')

# Comparar si son diferentes
print(
    f'Si valor_1 es {valor1} y valor_2 es {valor2} la comparacion de desiguldad es {valor1 != valor2}')


# Comparar si el primero es menor
print(
    f'Si valor_1 es {valor1} y valor_2 es {valor2} la comparacion de menor {valor1 < valor2}')

# Comparar si el primero es menor o igual
print(
    f'Si valor_1 es {valor1} y valor_2 es {valor2} la comparacion de menor o igual {valor1 <= valor2}')
