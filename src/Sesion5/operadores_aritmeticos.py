# Nuestros operadores aritmeticos son

variable_suma = 10 + 15
variable_resta = 20 - 36
variable_producto = 8 * 3
variable_division = 9 / 3
variable_division_entera = 7 // 3
variable_modulo = 7 % 5

print(f'''suma = {variable_suma}
resta = {variable_resta}
producto = {variable_producto}
division = {variable_division}
division entera = {variable_division_entera}
modulo = {variable_modulo}''')
