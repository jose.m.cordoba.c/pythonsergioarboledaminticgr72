"""Ejemplos operaciones lógicas
    """

# Variables de tipo Logico
con_bono = True
sin_bono = False

print(type(con_bono))

# Operador de negacion
print(f'Si con_bono vale {con_bono}, entonces sin_bono es {not con_bono}')
print(f'Si q vale {sin_bono}, entonces la negacion de q es {not sin_bono}')

# Operador de conjuncion "y"
print(
    f'Si p vale {con_bono} y q vale {sin_bono}, entonces la conjuncion de "p y q" es {con_bono and sin_bono}')

# Operador de disyuncion "o"
print(
    f'Si p vale {con_bono} y q vale {sin_bono}, entonces la disyuncion de "p o q" es {con_bono or sin_bono}')
