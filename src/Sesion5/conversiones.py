"""Ejemplos de conversiones
    """

# Creamos un texto
texto = '7584'
print(f' El siguiente dato = {texto}, es de tipo = {type(texto)}')

# Convertir un texto en numero entero
entero = int(texto)
print(f' El siguiente dato = {entero}, es de tipo = {type(entero)}')

# Convertir a un numero real
real = float(entero)
print(f' El siguiente dato = {real}, es de tipo = {type(real)}')

# Convertir un numero en texto
texto = str(real)  # str significa String 'Cadena de texto'
print(f' El siguiente dato = {texto}, es de tipo = {type(texto)}')
