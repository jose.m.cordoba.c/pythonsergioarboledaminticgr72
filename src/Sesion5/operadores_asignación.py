"""Programa para probar operadores de asignación
    """

otra_variable = 10
variable_muy_variable = otra_variable
print(f'Mi variable tiene el valor de {variable_muy_variable}')
variable_muy_variable += 15
print('Mi variable tiene el valor de', variable_muy_variable)
variable_muy_variable -= 5
print('Mi variable tiene el valor de', variable_muy_variable)
variable_muy_variable *= 6
print('Mi variable tiene el valor de', variable_muy_variable)
variable_muy_variable //= 10
print('Mi variable tiene el valor de', variable_muy_variable)
variable_muy_variable /= 1
print('Mi variable tiene el valor de', variable_muy_variable)
variable_muy_variable %= 7
print('Mi variable tiene el valor de', variable_muy_variable)
