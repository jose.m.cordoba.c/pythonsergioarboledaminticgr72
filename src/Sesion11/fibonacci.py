"""Programa que imprime los elementos de la serie de fibonacci

    """


elemento = int(
    input('Ingrese el numero de elementos de la serie de fibonacci a mostrar '))

contador, n, n_mas_uno = 0, 0, 1

while contador <= elemento:
    print(n)
    temp = n_mas_uno
    n_mas_uno += n
    n = temp
    contador += 1
