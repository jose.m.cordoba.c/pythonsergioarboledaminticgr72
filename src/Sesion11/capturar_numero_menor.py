"""Programa que captura un numero menor que 10
    """

# Mi while se va a ejecutar hasta que mi numero sea menor que 10
numero = 11

print('Vamos a leer numeros menores que 10')

while numero >= 10:
    numero = input('Ingrese un numero < 10 ')
    numero = float(numero)
    # if numero.isnumeric():
    #     numero = int(numero)
    # else:
    #     print(f'{numero} no es numero valido ')
    #     numero = 11

print(f'El numero {numero} es correcto')
