"""Suma todos los numeros ingresados por el usuario hasta que recibamos un 0
    """

acumulador = 0

while True:
    numero = int(input('Ingrese un numero '))
    if numero == 0:
        break
    acumulador += numero

print(f'El total acumulado es {acumulador} ')
