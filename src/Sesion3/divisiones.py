"""Programa para hacer divisiones
    """

division = 7 / 6
print(f'El resultado de division es {division}')
division = 70 / 87
print(f'El resultado de division es {division}')
division = 978 / 535411
print(f'El resultado de division es {division}')
division = 879879 / 6
print(f'El resultado de division es {division}')
division = 7 / 1  # Arrglado con el depurador no debo dividir por 0
print(f'El resultado de division es {division}')
division = 7 / 878979
print(f'El resultado de division es {division}')
division = 756454 / 6212
print(f'El resultado de division es {division}')
