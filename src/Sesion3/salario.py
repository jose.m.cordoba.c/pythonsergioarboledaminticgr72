"""Programa para calcular el salario de un trabajador dadas las horas laboradas
"""
nombre = input('Por favor ingrese el "nombre" del trabajador ')
identificacion = input(f'Por favor ingrese la identificacion para {nombre} ')
horas_trabajadas = input(
    f'Por favor ingrese las horas trabajadas para el usuario {identificacion} ')
horas_trabajadas = int(horas_trabajadas)

valor_hora = int(input('Por favor ingrese el valor de la hora trabajada '))

salario = horas_trabajadas * valor_hora

print(f'''El salario para el usuario: {nombre},
Identificado con: {identificacion},
Que trabajó {horas_trabajadas} horas,
Con un valor por hora de {valor_hora} pesos,
Es de ${salario}
''')
