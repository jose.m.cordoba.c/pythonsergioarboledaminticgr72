"""Ejemplos de Strings
    """


# Se pueden definis con comillas "" cuando quiero utilizar ' en mi texto
cadena = "I'm proud of my students"
print(cadena)

# Se pueden usar comillas '' cuando quiero usar " en mi texto
cadena = 'El correcaminos dice: "Beep Beep"'
print(cadena)

# Tambien puedo fugar caracters especiales
cadena = 'I\'m proud of my students'
print(cadena)

# Para saber cuantos caracteres hay en una cadena puedo usar len
print(f'Mi cadena tiene {len(cadena)} caracteres')

# Puedo acceder a los elementos de acuerdo a sus indices
print(cadena[2])
# Tambien puedo de desreca a izquierda usando números negativos
print(cadena[-2])

# Cada elemento de la cadena por indices
for indice in range(len(cadena)):
    print(cadena[indice])

print('\n\n\n')

# Y su inverso
for i in range(-1, -len(cadena) - 1, -1):
    print(cadena[i])

# sumar cadenas (concatenación)
saludo = 'hola ' + 'mundo '
print(saludo)

# Multiplicar la cadena por un entero
saludo_3_veces = saludo * 3
print(saludo_3_veces)

# Comparaciones entre cadenas
print(saludo == saludo_3_veces)
print(saludo != saludo_3_veces)
print(saludo >= saludo_3_veces)
print(saludo <= saludo_3_veces)

# para obtener el valor de un caracter usamos ord
print(ord("3"))
print(ord("3"))
print(ord("A"))
print(ord("a"))

# Para sacar una porción de una cadina "Slicing"
print(saludo[0:4])
print(saludo[:5] * 10)

print(saludo[5:])

# Conversión a mayusculas minusculas titulos y capitalizacion
print(saludo.upper())
print(saludo.lower())
print(saludo.capitalize())
print(saludo.title())

# Limpiando cadenas
cadena_sucia = '        Hola mundo         '
print(cadena_sucia.lstrip())
print(cadena_sucia.rstrip())
print(cadena_sucia.strip())

# Reemplazar
cadena2 = saludo.replace('hola', 'Bueeeenas')
print(cadena2)
