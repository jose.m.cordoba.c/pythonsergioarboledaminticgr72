""" Programa que determina si una cadena de un usuario comienza con otra
"""


def empieza_con(cadena: str, posible_subcadena: str) -> bool:
    """

    >>> empieza_con("hola mundo", "hola")
    True

    >>> empieza_con("hola mundo", "hola mundo")
    True

    >>> empieza_con("hola mundo", "mundo")
    False

    >>> empieza_con("mundo", "Hola mundo")
    False

    >>> empieza_con("hola", "hola mundo")
    False

    """
    return len(posible_subcadena) <= len(cadena) and cadena[:len(posible_subcadena)] == posible_subcadena
