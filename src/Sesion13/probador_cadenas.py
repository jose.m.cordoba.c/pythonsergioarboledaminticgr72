"""Programa para probar la funcion empieza con y otras de cadenas
"""

from subcadena import empieza_con

texto = input('Por favor ingrese una cadena: ')
posible_subcadena = input('Por favor ingrese otra cadena: ')

if empieza_con(texto, posible_subcadena):
    print(f'''El texto: "{texto}":
    empieza con: "{posible_subcadena}"
    ''')
else:
    print(f'"{texto}" no empieza con "{posible_subcadena}"')
