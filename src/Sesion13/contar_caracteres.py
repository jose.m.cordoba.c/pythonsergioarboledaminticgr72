"""Programa que permite contar elementos en una cadena
    """


def es_caracter(posible_caracter: str) -> bool:
    """

    >>> es_caracter("o")
    True

    >>> es_caracter("oer")
    False

    >>> es_caracter("")
    False

    >>> es_caracter(10)
    False

    """
    return type(posible_caracter) == str and len(posible_caracter) == 1


def contar_caracteres(texto: str, caracter: str) -> int:
    """

    >>> contar_caracteres("Hola mundo", "o")
    2

    >>> contar_caracteres("Jose Manuel Cordoba Castillo", "a")
    3

    >>> contar_caracteres("Jose Manuel Cordoba Castillo", "z")
    0

    >>> contar_caracteres("Jose Manuel Cordoba Castillo", "za")
    'No es un caracter valido'

    >>> contar_caracteres("Jose Manuel Cordoba Castillo", True)
    'No es un caracter valido'

    """
    if not es_caracter(caracter):
        return 'No es un caracter valido'

    contador = 0
    for letra in texto:
        if letra == caracter:
            contador += 1
    return contador


def contar_palabras(texto: str) -> int:
    """

    >>> contar_palabras("hola mundo")
    2

    >>> contar_palabras("Jose Manuel Cordoba Castillo")
    4

    >>> contar_palabras("")
    0

    >>> contar_palabras("Jose    Cordoba")
    2

    """
    return len(texto.split())


def reemplazar_caracter(texto: str, caracter_a_reemplazar: str, caracter_sustituto: str) -> str:
    """

    >>> reemplazar_caracter("perrito", "o", "a")
    'perrita'

    >>> reemplazar_caracter("anitalavalatina", "v", "b")
    'anitalabalatina'

    >>> reemplazar_caracter("anitalavalatina", "a", "z")
    'znitalavalatina'

    >>> reemplazar_caracter("anitalavalatina", 10, "b")
    '10 no es un caracter valido'

    >>> reemplazar_caracter("anitalavalatina", "v", False)
    'False no es un caracter valido'

    """
    if not es_caracter(caracter_a_reemplazar):
        return f'{caracter_a_reemplazar} no es un caracter valido'
    if not es_caracter(caracter_sustituto):
        return f'{caracter_sustituto} no es un caracter valido'
    posicion = texto.find(caracter_a_reemplazar)
    return texto[:posicion] + caracter_sustituto + texto[posicion + 1:]
