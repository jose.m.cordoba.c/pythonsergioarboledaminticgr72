"""Programa para calcular el interes de una inversion
    """

TASA_EFECTIVA_ANUAL = 0.15


def convertir_a_tasa_mensual(tasa_anual: float) -> float:
    """Convierte una tasa efectiva anual en NMV

    >>> convertir_a_tasa_mensual(0.15)
    0.01171491691985338

    >>> convertir_a_tasa_mensual(0.0)
    0.0

    >>> convertir_a_tasa_mensual(0.2)
    0.015309470499731193

    Args:
        tasa_anual (float): [description]

    Returns:
        float: [description]
    """
    return (1 + tasa_anual) ** (1/12) - 1


def calcular_rendimiento_mensual(valor_inversion: float, tasa_EA: float = TASA_EFECTIVA_ANUAL) -> float:
    """[summary]

    >>> calcular_rendimiento_mensual(100)
    1.17

    >>> calcular_rendimiento_mensual(0)
    0.0

    Args:
        valor_inversion (float): [description]
        tasa_EA (float, optional): [description]. Defaults to TASA_EFECTIVA_ANUAL.

    Returns:
        float: [description]
    """
    tasa_mensual = convertir_a_tasa_mensual(tasa_EA)
    return round(valor_inversion * tasa_mensual, 2)


inversión = float(input('Ingrese el valor de su inversión '))
rendimiento_mensual = calcular_rendimiento_mensual(inversión)
print(
    f'El rendimiento mensual para su inversión de {inversión} es de {rendimiento_mensual}')
