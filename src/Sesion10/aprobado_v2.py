"""Detirmina si un estudiante aprueba basado en su promedio
    """


def aprobo(nota1: int, nota2: int, nota3: int) -> str:
    """determina si un estuante aprueba un resultado

    >>> aprobo(80, 70, 90)
    'Muy bien'

    >>> aprobo(5, 50, 60)
    'Deficiente'

    >>> aprobo(60, 60, 60)
    'Casi pero no'

    >>> aprobo(100, 100, 100)
    'Perfecto'

    Args:
        nota1 (int): [description]
        nota2 (int): [description]
        nota3 (int): [description]

    Returns:
        bool: [description]
    """
    promedio = (nota1 + nota2 + nota3) / 3
    if promedio == 100:
        return "Perfecto"
    elif promedio >= 70:
        return "Muy bien"
    elif promedio >= 50:
        return "Casi pero no"
    else:
        return "Deficiente"
