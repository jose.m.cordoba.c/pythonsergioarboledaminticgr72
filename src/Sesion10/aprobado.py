"""Detirmina si un estudiante aprueba basado en su promedio
    """


def aprobo(nota1: int, nota2: int, nota3: int) -> bool:
    """determina si un estuante aprueba un resultado

    >>> aprobo(80, 70, 90)
    True

    >>> aprobo(5, 50, 60)
    False

    Args:
        nota1 (int): [description]
        nota2 (int): [description]
        nota3 (int): [description]

    Returns:
        bool: [description]
    """
    promedio = (nota1 + nota2 + nota3) / 3
    if promedio >= 70:
        return True
    else:
        return False
