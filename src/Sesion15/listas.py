"""Ejemplos de listas
    """

# definimos una lista
mi_lista = ['Jose Cordoba', 34, True, ['Tocar guitarra', 'cocinar']]
print(mi_lista)
print(type(mi_lista))

# Operadores de listas
otra_lista = [1, 2, 3]

# Concatenar listas
print(mi_lista + otra_lista)

# Multiplicar las listas
print(4 * otra_lista)

# Comparar listas
print(mi_lista == otra_lista)

print(mi_lista != otra_lista)

# Obtener elementos por posicion
print(mi_lista[0])

mi_lista[0] = 'Jose M Cordoba C'

print(mi_lista[0])

# Funciones de listas

# Longitud
print(len(mi_lista))

# agregar elementos
mi_lista.append("Pollo")
print(mi_lista)

# agregar en una poscicion especifica
mi_lista.insert(3, 72)
print(mi_lista)

# remover por posicion
mi_lista.remove(72)
print(mi_lista)


indice_cocinar = mi_lista[mi_lista.index(
    ['Tocar guitarra', 'cocinar'])].index('cocinar')
print(indice_cocinar)
