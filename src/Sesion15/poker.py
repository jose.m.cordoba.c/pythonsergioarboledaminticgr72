"""Ejercicio de cartas de poker
    """

# Una carta esta representada por una tupla ej. ("🧡", 10)


def es_poker(cartas: list) -> bool:
    """Determina si una combinacion de cartas es un poker

    >>> es_poker([("🧡", 1), ("♦️", 1), ("♣️", 1), ("♠️", 1), ("🧡", 2)])
    True

    >>> es_poker([("🧡", 11), ("♦️", 11),("🧡", 2), ("♣️", 11), ("♠️", 11) ])
    True

    >>> es_poker([("🧡", 11), ("🧡", 11),("🧡", 2), ("♣️", 11), ("♠️", 11) ])
    False

    >>> es_poker([("🧡", 11), ("♦️", 11),("🧡", 2), ("♣️", 12), ("♠️", 11) ])
    False

    Args:
        cartas (list of tuplas len == 5): Las cartas en la mano

    Returns:
        bool: True si 4 de las 5 cartas tienen el mismo numero
    """
    return es_poker_4_cartas(cartas[0], cartas[1], cartas[2], cartas[3]) \
        or es_poker_4_cartas(cartas[1], cartas[2], cartas[3], cartas[4]) \
        or es_poker_4_cartas(cartas[2], cartas[3], cartas[4], cartas[0]) \
        or es_poker_4_cartas(cartas[3], cartas[4], cartas[0], cartas[1]) \
        or es_poker_4_cartas(cartas[4], cartas[0], cartas[1], cartas[2])


def es_poker_4_cartas(carta1: tuple, carta2: tuple, carta3: tuple, carta4: tuple) -> bool:
    """Determina si 4 cartas forman un poker

    >>> es_poker_4_cartas(("🧡", 1), ("♦️", 1), ("♣️", 1), ("♠️", 1))
    True

    >>> es_poker_4_cartas(("🧡", 11), ("♦️", 11), ("♣️", 11), ("♠️", 11))
    True

    >>> es_poker_4_cartas(("🧡", 11), ("🧡", 11), ("♣️", 11), ("♠️", 11))
    False

    Args:
        carta1 (tuple): [description]
        carta2 (tuple): [description]
        carta3 (tuple): [description]
        carta4 (tuple): [description]

    Returns:
        bool: [description]
    """
    return carta1[1] == carta2[1] == carta3[1] == carta4[1] and carta1[0] != carta2[0] != carta3[0] != carta4[0]
