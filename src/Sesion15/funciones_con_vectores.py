"""Ejemplos de funciones con vectores"""


def vector_por_escalar(vector: list, escalar: int) -> list:
    """
    >>> vector_por_escalar([1, 2, 3], 3)
    [3, 6, 9]

    >>> vector_por_escalar([1, 2, 3], 0)
    [0, 0, 0]

    >>> vector_por_escalar([1, 2, 3], 1)
    [1, 2, 3]

    """
    vector_resultante = []
    for elemento in vector:
        vector_resultante.append(elemento * escalar)
    return vector_resultante


def producto_punto(v1: list, v2: list) -> float:
    """

    >>> producto_punto([1, 2, 3], [3, 2, 1])
    10

    >>> producto_punto([1, 2, 3], [0, 0, 0])
    0

    >>> producto_punto([1, 2, 3], [1, 1, 1])
    6

    """
    acumulador = 0
    for i in range(len(v1)):
        acumulador += v1[i] * v2[i]
    return acumulador


def son_ortogonales(v1: list, v2: list) -> bool:
    """

    >>> son_ortogonales([1, 2, 0], [2, -1, 10])
    True

    """
    return 0 == producto_punto(v1, v2)


def son_paralelos(v1: list, v2: list) -> bool:
    pass
