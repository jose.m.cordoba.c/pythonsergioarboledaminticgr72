def generar_diccionario(n: int) -> dict:
    """

    >>> generar_diccionario(2)
    {0: 0, 1: 1, 2: 4}

    >>> generar_diccionario(5)
    {0: 0, 1: 1, 2: 4, 3: 9, 4: 16, 5: 25}

    """
    dict_resultante = {}
    for i in range(n + 1):
        dict_resultante[i] = i ** 2
    return dict_resultante
