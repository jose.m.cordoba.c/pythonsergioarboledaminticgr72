"""Ejemplos con diccionarios"""

# Como crear un diccionario
mi_diccionario = {}

# Agregar un elemento
mi_diccionario['llave'] = 'valor'

print(mi_diccionario)

# Consultar un elemento con la llave
print(mi_diccionario['llave'])

mis_datos = {
    'nombre': 'Jose',
    'apellido': 'Cordoba',
    'Hobby': ['Cocinar', 'Tocar guitarra', 'Historia'],
    'edad': 34,
    'familia': {
        'Gato': 'karoo',
        'Gata': 'Paty',
        'Esposa': 'Vivi'
    }
}

print(mis_datos)

mis_datos['apellido'] = 'Cordoba Castillo'

print(mis_datos)

mis_datos.update(mi_diccionario)

print(mis_datos)


