diccionario_preguntas = {
    "Hola": "Buen día Amo",
    "Como Estas": "Me encuentro bien binario"
}


while True:
    entrada = input('Preguntame algo o di salir para terminar').lower()
    if entrada == "salir":
        break
    elif entrada in diccionario_preguntas:
        respuesta = diccionario_preguntas[entrada]
        print(respuesta.capitalize())
    else:
        respuesta = input('Como deberia responder a eso?')
        diccionario_preguntas[entrada] = respuesta
        print(
            f'Listo aprendi que para la pregunta: "{entrada.capitalize()}" debo responder "{respuesta.capitalize()}"')

print('Suerte! ')
