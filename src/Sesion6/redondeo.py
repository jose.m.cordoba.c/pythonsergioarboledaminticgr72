"""Ejemplo de como redondear cifras
"""

flotante1 = 789.49164
flotante2 = 789.5

flotante1 = round(flotante1, 1)

if flotante1 == flotante2:
    print('El redondeo funciona', flotante1)
else:
    print('El redondeo no funcionó')


# FIXME encontrar una solucion para la división por cero
flotante2 /= flotante1
