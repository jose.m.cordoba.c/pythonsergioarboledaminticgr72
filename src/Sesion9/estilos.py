"""Archivo de ejemplos de estilos
    """

from math import pi
__author__ = "Jose Cordoba"


# import ../Sesion7/nota_definitiva


def una_funcion_con_un_nombre_extremadamente_largo(un_parametro, otro_paramentro, un_parametro_mas, el_ultimo_parametro, este_si_es_el_ultimo_parametro) -> str:

    # Cuando tenemos una operacion mas larga que 72 caracteres podemos utiliza \ como conector de lineas
    x = un_parametro + otro_paramentro + un_parametro_mas  \
        + el_ultimo_parametro + este_si_es_el_ultimo_parametro
    pass


def segunda_funcion():
    pass


def tercera_funcion():
    pass

# from math import pi


class Gato:

    dormido = True

    def ronronear(self, tiempo):
        pass
