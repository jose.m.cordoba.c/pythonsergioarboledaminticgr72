""""""


def es_par(n: int) -> bool:
    """

    >>> es_par(0)
    True

    >>> es_par(1)
    False

    >>> es_par(17)
    False

    >>> es_par(36)
    True

    """
    if n == 0:
        return True
    elif n == 1:
        return False
    else:
        return es_par(n - 2)


es_par(7)
