def buscar_indices(texto: str, sub: str, inicio=0) -> list:
    """

    >>> buscar_indices("Un tete en la tete con Tete", "te")
    [3, 5, 10, 12, 21]

    """
    try:
        indice = texto.index(sub)
    except ValueError:  # si no encontro el texto maneje el error
        indice = -1
    if indice == -1:
        return []
    return [indice + inicio] + buscar_indices(texto[indice + len(sub):], sub, inicio + indice)


buscar_indices("Un tete en la tete con Tete", "te")
