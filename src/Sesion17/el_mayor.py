def el_mayor(lista: list) -> object:
    """

    >>> el_mayor([1, 2, 5, 90, 3, 7, 4])
    90

    """
    if len(lista) == 1:
        return lista[0]

    el_mayor_sublista = el_mayor(lista[1:])

    return lista[0] if lista[0] > el_mayor_sublista else el_mayor_sublista


el_mayor([1, 2, 5, 90, 3, 7, 4])
