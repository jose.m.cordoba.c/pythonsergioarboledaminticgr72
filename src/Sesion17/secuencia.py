def secuencia(n):
    if n < 3:
        return n
    return secuencia(n - 3)


for i in range(10):
    print(secuencia(i))
