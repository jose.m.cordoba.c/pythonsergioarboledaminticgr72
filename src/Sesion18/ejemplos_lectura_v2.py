"""Ejemplos con lectura de archivos"""


# Abrir el archivo (No Olvidar el modo en el que se abre 'r', 'w', 'a')
archivo = open('src/Sesion18/ejemplo', 'r')

# Leer el archivo linea por linea
contador = 1
for linea in archivo:
    print(f'{contador}. {linea.strip()}')
    contador += 1

# Cerrar el archivo
archivo.close()
