"""Ejemplos con lectura de archivos"""


# Abrir el archivo (No Olvidar el modo en el que se abre 'r', 'w', 'a')
archivo = open('src/Sesion18/ejemplo', 'r')

# Leer el archivo
print(archivo.read())

# Cerrar el archivo
archivo.close()
