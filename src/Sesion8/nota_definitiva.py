"""Programa para calcular la nota final de un estudiante
    """


def calcular_nota(taller1, taller2, taller3,
                  quiz1, quiz2,
                  trabajo_final, sustentacion) -> float:
    """Calcula la nota definitiva basado en los resultados 
    parciales

    La nota del promedio de los talleres es el 30% de la nota final

    >>> calcular_nota(10, 10, 10, 10, 10, 10, 10)
    10.0

    >>> calcular_nota(10, 10, 10, 10, 10, 10, 0)
    8.0

    >>> calcular_nota(0, 0, 0, 0, 0, 0, 0)
    0.0

    Args:
        taller1 (float): Representa el 10% del resultado
        taller2 (float): Representa el 10% del resultado
        taller3 (float): Representa el 10% del resultado
        quiz1 (float): Representa el 15% del resultado
        quiz2 (float): Representa el 15% del resultado
        trabajo_final (float): Representa el 20% del resultado
        sustentacion (float): Representa el 20% del resultado

    Returns:
        float: la nota final
    """
    nota_final = 0

    # Calculo la nota de los talleres
    nota_talleres = (taller1 + taller2 + taller3) * 0.1
    nota_final += nota_talleres

    # Calcular la nota de los quizes
    nota_quizes = (quiz1 + quiz2) * 0.15
    nota_final += nota_quizes

    # Agregar el resultado del trabajo final
    nota_final += (trabajo_final + sustentacion) * 0.2
    return nota_final
