# Produciendo un error
try:
    x = 10 / 0
except ZeroDivisionError:
    print('Opps no puedo dividir por 0')

# Hacer una funcion division que maneje el error dividir por 0

lista = [1, 2, 3]
try:
    print(lista[3])
except IndexError:
    print('No existe ese indice en la lista')

print('hola ' + 10)
